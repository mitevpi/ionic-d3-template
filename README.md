# Ionic D3 Template

Template for working with D3 inside Ionic V4.

Currently the template is limited to the basic [Bar Chart Page](/src/app/bar-chart).

## Build and Run

Download/clone the repository, and then run the following commands in the root directory.

```cmd
npm i
ionic serve
```