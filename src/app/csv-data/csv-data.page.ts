import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-csv-data',
  templateUrl: './csv-data.page.html',
  styleUrls: ['./csv-data.page.scss']
})
export class CsvDataPage implements OnInit {
  constructor(private http: Http) {}

  private readCsvData() {
    this.http
      .get('../../assets/data/dummyData.csv')
      .subscribe(data => this.extractData(data), err => this.handleError(err));
  }

  private handleError(err) {
    console.log('something went wrong: ', err);
  }

  private extractData(res) {
    console.log(res);
  }

  ngOnInit() {
    this.readCsvData();
  }
}
