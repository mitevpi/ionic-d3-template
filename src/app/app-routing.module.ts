import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'BarChart', loadChildren: './bar-chart/bar-chart.module#BarChartPageModule' },
  { path: 'csv-data', loadChildren: './csv-data/csv-data.module#CsvDataPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
