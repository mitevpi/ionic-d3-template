import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { IonicModule } from '@ionic/angular';

import { CsvDataPage } from './csv-data.page';

const routes: Routes = [
  {
    path: '',
    component: CsvDataPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CsvDataPage]
})
export class CsvDataPageModule {}
