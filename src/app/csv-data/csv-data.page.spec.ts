import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvDataPage } from './csv-data.page';

describe('CsvDataPage', () => {
  let component: CsvDataPage;
  let fixture: ComponentFixture<CsvDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsvDataPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
